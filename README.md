Deep Dreams
===========

This is the code that generates [Deep Dreams podcast
episodes](https://deepdreams.stavros.io) from a script.

Since this is all very ad-hoc and bound to change at any time, there isn't much
documentation, but generally:

* Run `./gen_script.py <topic>` to generate your episode's script.
* If the script in `script.txt` looks good, move it to `episodes/episodeNN.txt`.
* Run `./gen_audio.py <episode_number>`.
* Upload the file from `audio/episode-NN.mp3` to Anchor
  (https://podcasters.spotify.com/pod/dashboard/home, log in with email).
* Push the new episode to the repo upstream (this autogenerates the site from Anchor).

To use other models (via OpenRouter), run the script like so:

```
./gen_script.py --model=teknium/openhermes-2-mistral-7b --api-base="https://openrouter.ai/api/v1" <prompt>
```
