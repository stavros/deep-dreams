#!/usr/bin/env python3
"""
Get ChatGPT to generate some text.

Because gen_script didn't work so well with newer models, this script uses a new method:

It first asks ChatGPT to write an outline for the story (in acts and scenes), and then
tells it to write each scene separately, feeding it the outline and previous scene.
"""

import argparse
import math
import os
import re
import unicodedata
from pathlib import Path
from typing import List
from typing import Tuple

from colorama import Fore
from colorama import Style
from openai import OpenAI


client = OpenAI(api_key=os.environ["OPENAI_API_KEY"])


def retry(times: int):
    """Retry a function `times` times."""

    def decorator(func):
        def inner_func(*args, **kwargs):
            exc = None
            for _ in range(times):
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    exc = e
                    print("There was an error, retrying...")
            else:
                if exc:
                    raise exc

        return inner_func

    return decorator


def count_tokens(text: str) -> int:
    """Approximate the number of tokens in `text`."""
    return int(len(re.findall(r"""(\w+|[\.';\!\?:,\"]+)""", text)) * 1.1)


def normalize(text: str) -> str:
    return unicodedata.normalize("NFKD", text.strip())


@retry(3)
def ask(messages, model):
    completion = client.chat.completions.create(
        model=model,
        messages=[
            {
                "role": "system",
                "content": "You are an excellent writer of fairy tales.",
            },
        ]
        + messages,
    )

    content = completion.choices[0].message.content.strip()
    return content


class ScriptWriter:
    def __init__(self, topic, model, acts, scenes):
        self._topic_prompt = (
            f"Write an outline of a fairy tale for adults about {topic}. It should "
            f"comprise {acts} acts, with {scenes} scenes per act. Each "
            "scene outline should be one or two sentences long."
        )
        self._outline: str = ""
        self._scenes: List[str] = []
        self._model = model
        self._acts = acts
        self._scenes_per_act = scenes

    def _counter_to_act_scene(self, number: int) -> Tuple[int, int]:
        """Convert a counter to an (act, scene) tuple."""
        return (
            math.ceil(number / self._scenes_per_act),
            ((number - 1) % self._scenes_per_act) + 1,
        )

    def write_outline(self):
        """Ask the model to write the outline of the story."""
        self._outline = ask(
            [{"role": "user", "content": self._topic_prompt}],
            model=self._model,
        )

    def write_scene(self):
        """Ask the model to write a scene."""
        messages = [
            {"role": "user", "content": self._topic_prompt},
            {"role": "assistant", "content": self._outline},
        ]

        for counter in range(2):
            if counter == 0 and not self._scenes:
                continue
            act, scene = self._counter_to_act_scene(len(self._scenes) + counter)
            messages.append(
                {
                    "role": "user",
                    "content": f"Now please write the whole text for act "
                    f"{act}, scene {scene}. It should be great writing, funny, with "
                    "dry wit, and scathing lines that adults will find sardonically "
                    "funny and clever. Write just the scene's text in prose, not in "
                    "script or theatrical format. Do not output anything other than "
                    "scene text.",
                }
            )
        scene = ask(messages, model=self._model)
        self._scenes.append(scene)

    def get_whole_story(self) -> str:
        """Return the whole story."""
        story = "\n\n".join(self._scenes).strip()
        # Strip spaces from the ends of lines.
        story = re.sub(" *\n", "\n", story)
        return story


def main(filename: Path, topic, model, acts, scenes) -> None:
    print(Fore.GREEN + "Prompt:" + Style.RESET_ALL)
    print(f"A script about {topic}.\n")

    writer = ScriptWriter(topic, model, acts, scenes)
    writer.write_outline()

    print(Fore.GREEN + "Outline:" + Style.RESET_ALL)
    print(writer._outline + "\n")

    for counter in range(acts * scenes):
        writer.write_scene()

        act, scene = writer._counter_to_act_scene(len(writer._scenes))
        print(Fore.GREEN + f"Act {act}, scene {scene}:" + Style.RESET_ALL)
        print(writer._scenes[-1] + "\n")

    with filename.open("w") as script:
        script.write(normalize(writer.get_whole_story()))

    token_count = count_tokens(writer.get_whole_story())
    print(f"Tokens: {Fore.GREEN}{token_count}{Style.RESET_ALL}")
    print(f"Estimated time: {Fore.GREEN}{int(token_count / 190)} min{Style.RESET_ALL}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a script.")
    parser.add_argument("topic", help="the topic the script is about")
    parser.add_argument(
        "-m", "--model", type=str, default="gpt-3.5-turbo", help="the model to use"
    )
    parser.add_argument(
        "--api-base", type=str, default=None, help="the base URL for the OpenAI API"
    )
    parser.add_argument("--acts", type=int, default=3, help="number of acts")
    parser.add_argument(
        "--scenes", type=int, default=4, help="number of scenes per act"
    )
    args = parser.parse_args()

    if args.api_base:
        client = OpenAI(api_key=os.environ["OPENAI_API_KEY"], base_url=args.api_base)

    file = Path("script.txt")
    main(file, topic=args.topic, model=args.model, acts=args.acts, scenes=args.scenes)
